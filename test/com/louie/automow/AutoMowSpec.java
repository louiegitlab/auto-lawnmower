package com.louie.automow;

import com.louie.automow.io.input.InputDTO;
import com.louie.automow.movable.Movable;
import com.louie.automow.movable.Movement;
import com.louie.automow.movable.Orientation;
import com.louie.automow.position.Coordinate;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class AutoMowSpec {
	private InputDTO.Builder inputDTO;
	private final Queue<Movement> movements1 = new LinkedList<>(Arrays.asList(
			Movement.TURN_LEFT,
			Movement.MOVE_FORWARD,
			Movement.TURN_LEFT,
			Movement.MOVE_FORWARD,
			Movement.TURN_LEFT,
			Movement.MOVE_FORWARD,
			Movement.TURN_LEFT,
			Movement.MOVE_FORWARD,
			Movement.MOVE_FORWARD));

	private final Queue<Movement> movements2 = new LinkedList<>(Arrays.asList(
			Movement.MOVE_FORWARD,
			Movement.MOVE_FORWARD,
			Movement.TURN_RIGHT,
			Movement.MOVE_FORWARD,
			Movement.MOVE_FORWARD,
			Movement.TURN_RIGHT,
			Movement.MOVE_FORWARD,
			Movement.TURN_RIGHT,
			Movement.TURN_RIGHT,
			Movement.MOVE_FORWARD));

	@BeforeEach
	void reset() {
		this.inputDTO = new InputDTO.Builder().addLawn(new Coordinate(5, 5));
	}

	@Test
	void noMowers() {
		assertTrue(AutoMow.autoMow(inputDTO.build()).isEmpty());
	}

	@Test
	void oneMower() {
		InputDTO input = inputDTO
			.addMowerInstructions(new Coordinate(1, 2), Orientation.NORTH, movements1)
			.build();
		List<Movable> movables = AutoMow.autoMow(input);
		assertEquals(movables.size(), 1);

		Movable movable = movables.get(0);
		assertEquals(movable.getCoordinate(), new Coordinate(1, 3));
		assertEquals(movable.getOrientation(), Orientation.NORTH);
	}

	@Test
	void twoMowers() {
		InputDTO input = inputDTO
			.addMowerInstructions(new Coordinate(1, 2), Orientation.NORTH, movements1)
			.addMowerInstructions(new Coordinate(3, 3), Orientation.EAST, movements2)
			.build();
		List<Movable> movables = AutoMow.autoMow(input);
		assertEquals(movables.size(), 2);

		Movable movable = movables.get(0);
		assertEquals(movable.getCoordinate(), new Coordinate(1, 3));
		assertEquals(movable.getOrientation(), Orientation.NORTH);

		movable = movables.get(1);
		assertEquals(movable.getCoordinate(), new Coordinate(5, 1));
		assertEquals(movable.getOrientation(), Orientation.EAST);
	}
}