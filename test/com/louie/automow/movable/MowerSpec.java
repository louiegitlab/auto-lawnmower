package com.louie.automow.movable;

import com.louie.automow.lawn.Lawn;
import com.louie.automow.lawn.SquareLawn;
import com.louie.automow.position.Coordinate;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class MowerSpec {
	private Mower mower;
	private Lawn lawn;
	private Coordinate startingLocation;
	private Orientation startingDirection;

	@BeforeEach
	void reset() {
		lawn = new SquareLawn(new Coordinate(0, 0), new Coordinate(10, 10));
		startingLocation = new Coordinate(5, 5);
		startingDirection = Orientation.NORTH;
		mower = putMowerOnLawnWithCoordinates(startingLocation);
	}

	@Test
	void cannotPlaceOutsideOfLawn() {
		MovableException e = assertThrows(MovableException.class, () -> putMowerOnLawnWithCoordinates(new Coordinate(11, 11)));
		assertEquals(e.getMessage(), "Attempt to place mower (11, 11) outside of lawn");
	}

	@Test
	void turnLeft() {
		assertEquals(mower.turnLeft().getOrientation(), Orientation.WEST);
		assertEquals(mower.turnLeft().getOrientation(), Orientation.SOUTH);
		assertEquals(mower.turnLeft().getOrientation(), Orientation.EAST);
		assertEquals(mower.turnLeft().getOrientation(), Orientation.NORTH);
	}

	@Test
	void turnRight() {
		assertEquals(mower.turnRight().getOrientation(), Orientation.EAST);
		assertEquals(mower.turnRight().getOrientation(), Orientation.SOUTH);
		assertEquals(mower.turnRight().getOrientation(), Orientation.WEST);
		assertEquals(mower.turnRight().getOrientation(), Orientation.NORTH);
	}

	@Test
	void moveForward() {
		mower = putMowerOnLawnWithCoordinates(new Coordinate(10, 10));

		assertEquals(mower.moveForward().getCoordinate(), new Coordinate(10, 10));
		assertEquals(mower.turnRight().moveForward().getCoordinate(), new Coordinate(10, 10));
		assertEquals(mower.turnRight().moveForward().getCoordinate(), new Coordinate(10, 9));
		assertEquals(mower.turnRight().moveForward().getCoordinate(), new Coordinate(9, 9));
	}

	private Mower putMowerOnLawnWithCoordinates(Coordinate position) {
		return new Mower(lawn, position, startingDirection);
	}
}