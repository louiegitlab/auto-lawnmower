package com.louie.automow.movable;

import com.louie.automow.lawn.Lawn;
import com.louie.automow.lawn.SquareLawn;
import com.louie.automow.position.Coordinate;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ControlSpec {
	private Mower mower;

	@BeforeEach
	void reset() {
		Lawn lawn = new SquareLawn(new Coordinate(0, 0), new Coordinate(10, 10));
		mower = new Mower(lawn, new Coordinate(9, 5), Orientation.NORTH);
	}

	@Test
	void addMovements() {
		Control control = new Control(mower);
		assertTrue(control.getMovements().isEmpty());

		control.queueMovement(Movement.MOVE_FORWARD);
		assertEquals(control.getMovements().size(), 1);

		Queue<Movement> queue = new LinkedList<>(Arrays.asList(
				Movement.MOVE_FORWARD,
				Movement.TURN_LEFT,
				Movement.MOVE_FORWARD,
				Movement.TURN_RIGHT));
		control.queueMovement(queue);
		assertEquals(control.getMovements().size(), 5);
	}

	@Test
	void move() {
		Queue<Movement> queue = new LinkedList<>(Arrays.asList(
				Movement.MOVE_FORWARD,
				Movement.TURN_RIGHT,
				Movement.MOVE_FORWARD,
				Movement.MOVE_FORWARD,
				Movement.TURN_LEFT,
				Movement.TURN_LEFT));
		Control control = new Control(mower);
		assertTrue(control.getMovements().isEmpty());
		control.queueMovement(queue);

		control.move();

		assertTrue(control.getMovements().isEmpty());
		Mower mower = (Mower) control.getMovable();
		assertEquals(mower.getOrientation(), Orientation.WEST);
		assertEquals(mower.getCoordinate(), new Coordinate(10, 6));
	}
}