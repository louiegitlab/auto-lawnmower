package com.louie.automow.movable;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class OrientationSpec {
	@Test
	void testRight90Degrees() {
		assertEquals(Orientation.right90Degrees(Orientation.NORTH), Orientation.EAST);
		assertEquals(Orientation.right90Degrees(Orientation.EAST), Orientation.SOUTH);
		assertEquals(Orientation.right90Degrees(Orientation.SOUTH), Orientation.WEST);
		assertEquals(Orientation.right90Degrees(Orientation.WEST), Orientation.NORTH);
	}

	@Test
	void testLeft90Degrees() {
		assertEquals(Orientation.left90Degrees(Orientation.NORTH), Orientation.WEST);
		assertEquals(Orientation.left90Degrees(Orientation.WEST), Orientation.SOUTH);
		assertEquals(Orientation.left90Degrees(Orientation.SOUTH), Orientation.EAST);
		assertEquals(Orientation.left90Degrees(Orientation.EAST), Orientation.NORTH);
	}
}