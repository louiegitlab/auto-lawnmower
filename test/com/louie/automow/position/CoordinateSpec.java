package com.louie.automow.position;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


class CoordinateSpec {

	@Test
	void testEqualsSymmetric() {
		Coordinate coordinateA = new Coordinate(9, -4);
		Coordinate coordinateB = new Coordinate(9, -4);
		Coordinate coordinateC = new Coordinate(9, -3);

		assertTrue(coordinateA.equals(coordinateB) && coordinateB.equals(coordinateA));
		assertTrue(!coordinateA.equals(coordinateC) && !coordinateC.equals(coordinateA));
		assertEquals(coordinateA.hashCode(), coordinateB.hashCode());
	}

	@Test
	void coordinateToString() {
		assertEquals(new Coordinate(99, -33).toString(), "(99, -33)");

	}
}