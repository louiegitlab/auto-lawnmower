package com.louie.automow.io.input;

import com.louie.automow.io.FileException;
import com.louie.automow.movable.Movement;
import com.louie.automow.movable.Orientation;
import com.louie.automow.position.Coordinate;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.Arrays;
import java.util.LinkedList;

import static com.louie.automow.fixtures.TestFixtureHelpers.INPUT_FIXTURES_DIR;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class InputFileParserSpec {
	@Test
	void readNonExistentFile() {
		InputParser<File> inputFileParser = new InputFileParser();
		assertThrows(FileException.class, () -> inputFileParser.parse(new File(INPUT_FIXTURES_DIR + "nonexistent")));
	}

	@Test
	void extraNewLines() {
		InputParser<File> inputFileParser = new InputFileParser();
		assertThrows(FileException.class, () -> inputFileParser.parse(new File(INPUT_FIXTURES_DIR + "extra_new_lines")));
	}

	@Test
	void onlyLawn() {
		InputParser<File> inputFileParser = new InputFileParser();
		InputDTO inputDTO = inputFileParser.parse(new File(INPUT_FIXTURES_DIR + "only_lawn"));
		assertEquals(inputDTO.bottomLeftOfLawn, new Coordinate(0, 0));
		assertEquals(inputDTO.topRightOfLawn, new Coordinate(5, 5));
		assertTrue(inputDTO.mowerInstructionDTOs.isEmpty());
	}

	@Test
	void oneMower() {
		InputParser<File> inputFileParser = new InputFileParser();
		InputDTO inputDTO = inputFileParser.parse(new File(INPUT_FIXTURES_DIR + "one_mower"));

		assertEquals(inputDTO.bottomLeftOfLawn, new Coordinate(0, 0));
		assertEquals(inputDTO.topRightOfLawn, new Coordinate(5, 5));
		assertEquals(inputDTO.mowerInstructionDTOs.size(), 1);
		MowerInputDTO mowerInstructionDTO = inputDTO.mowerInstructionDTOs.get(0);
		assertEquals(mowerInstructionDTO.startingLocation, new Coordinate(1, 2));
		assertEquals(mowerInstructionDTO.orientation, Orientation.NORTH);
		assertEquals(mowerInstructionDTO.movement, new LinkedList<>(Arrays.asList(
				Movement.TURN_LEFT,
				Movement.MOVE_FORWARD,
				Movement.TURN_LEFT,
				Movement.MOVE_FORWARD,
				Movement.TURN_LEFT,
				Movement.MOVE_FORWARD,
				Movement.TURN_LEFT,
				Movement.MOVE_FORWARD,
				Movement.MOVE_FORWARD)));
	}

	@Test
	void incompleteMower() {
		InputParser<File> inputFileParser = new InputFileParser();
		InputDTO inputDTO = inputFileParser.parse(new File(INPUT_FIXTURES_DIR + "incomplete_second_mower"));

		assertEquals(inputDTO.bottomLeftOfLawn, new Coordinate(0, 0));
		assertEquals(inputDTO.topRightOfLawn, new Coordinate(5, 5));
		assertEquals(inputDTO.mowerInstructionDTOs.size(), 2);
		MowerInputDTO mowerInstructionDTO = inputDTO.mowerInstructionDTOs.get(0);
		assertEquals(mowerInstructionDTO.startingLocation, new Coordinate(1, 2));
		assertEquals(mowerInstructionDTO.orientation, Orientation.NORTH);
		assertEquals(mowerInstructionDTO.movement, new LinkedList<>(Arrays.asList(
				Movement.TURN_LEFT,
				Movement.MOVE_FORWARD,
				Movement.TURN_LEFT,
				Movement.MOVE_FORWARD,
				Movement.TURN_LEFT,
				Movement.MOVE_FORWARD,
				Movement.TURN_LEFT,
				Movement.MOVE_FORWARD,
				Movement.MOVE_FORWARD)));

		mowerInstructionDTO = inputDTO.mowerInstructionDTOs.get(1);
		assertEquals(mowerInstructionDTO.startingLocation, new Coordinate(3, 3));
		assertEquals(mowerInstructionDTO.orientation, Orientation.EAST);
		assertEquals(mowerInstructionDTO.movement, new LinkedList<>());
	}

	@Test
	void twoMowers() {
		InputParser<File> inputFileParser = new InputFileParser();
		InputDTO inputDTO = inputFileParser.parse(new File(INPUT_FIXTURES_DIR + "two_mowers"));

		assertEquals(inputDTO.bottomLeftOfLawn, new Coordinate(0, 0));
		assertEquals(inputDTO.topRightOfLawn, new Coordinate(5, 5));
		assertEquals(inputDTO.mowerInstructionDTOs.size(), 2);
		MowerInputDTO mowerInstructionDTO = inputDTO.mowerInstructionDTOs.get(0);
		assertEquals(mowerInstructionDTO.startingLocation, new Coordinate(1, 2));
		assertEquals(mowerInstructionDTO.orientation, Orientation.NORTH);
		assertEquals(mowerInstructionDTO.movement, new LinkedList<>(Arrays.asList(
				Movement.TURN_LEFT,
				Movement.MOVE_FORWARD,
				Movement.TURN_LEFT,
				Movement.MOVE_FORWARD,
				Movement.TURN_LEFT,
				Movement.MOVE_FORWARD,
				Movement.TURN_LEFT,
				Movement.MOVE_FORWARD,
				Movement.MOVE_FORWARD)));

		mowerInstructionDTO = inputDTO.mowerInstructionDTOs.get(1);
		assertEquals(mowerInstructionDTO.startingLocation, new Coordinate(3, 3));
		assertEquals(mowerInstructionDTO.orientation, Orientation.EAST);
		assertEquals(mowerInstructionDTO.movement, new LinkedList<>(Arrays.asList(
				Movement.MOVE_FORWARD,
				Movement.MOVE_FORWARD,
				Movement.TURN_RIGHT,
				Movement.MOVE_FORWARD,
				Movement.MOVE_FORWARD,
				Movement.TURN_RIGHT,
				Movement.MOVE_FORWARD,
				Movement.TURN_RIGHT,
				Movement.TURN_RIGHT,
				Movement.MOVE_FORWARD)));
	}
}