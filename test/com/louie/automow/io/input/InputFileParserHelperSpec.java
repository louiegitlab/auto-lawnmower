package com.louie.automow.io.input;

import com.louie.automow.io.FileException;
import com.louie.automow.movable.Movement;
import com.louie.automow.movable.Orientation;
import com.louie.automow.position.Coordinate;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.LinkedList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class InputFileParserHelperSpec {
	@Test
	void parseLawnCoordinates() {
		assertThrows(FileException.class, () -> InputFileParserHelper.parseLawnCoordinates(null));
		assertThrows(FileException.class, () -> InputFileParserHelper.parseLawnCoordinates(""));
		assertThrows(FileException.class, () -> InputFileParserHelper.parseLawnCoordinates("1"));
		assertThrows(FileException.class, () -> InputFileParserHelper.parseLawnCoordinates("1, 1"));
		assertThrows(FileException.class, () -> InputFileParserHelper.parseLawnCoordinates("1 1 1"));
		assertThrows(FileException.class, () -> InputFileParserHelper.parseLawnCoordinates("1 L"));
		assertThrows(FileException.class, () -> InputFileParserHelper.parseLawnCoordinates("L L"));

		assertEquals(InputFileParserHelper.parseLawnCoordinates("1 2"), new Coordinate(1, 2));
	}

	@Test
	void parseMowerCoordinates() {
		assertThrows(FileException.class, () -> InputFileParserHelper.parseLawnCoordinates(null));
		assertThrows(FileException.class, () -> InputFileParserHelper.parseMowerCoordinates(""));
		assertThrows(FileException.class, () -> InputFileParserHelper.parseMowerCoordinates("1 1"));
		assertThrows(FileException.class, () -> InputFileParserHelper.parseMowerCoordinates("1, 1, N"));
		assertThrows(FileException.class, () -> InputFileParserHelper.parseMowerCoordinates("1 1 N 1"));
		assertThrows(FileException.class, () -> InputFileParserHelper.parseMowerCoordinates("N N N"));

		assertEquals(InputFileParserHelper.parseMowerCoordinates("1 2 S"), new Coordinate(1, 2));
	}

	@Test
	void parseMowerOrientation() {
		assertThrows(FileException.class, () -> InputFileParserHelper.parseLawnCoordinates(null));
		assertThrows(FileException.class, () -> InputFileParserHelper.parseMowerOrientation(""));
		assertThrows(FileException.class, () -> InputFileParserHelper.parseMowerOrientation("1 1"));
		assertThrows(FileException.class, () -> InputFileParserHelper.parseMowerOrientation("1 1 1"));
		assertThrows(FileException.class, () -> InputFileParserHelper.parseMowerOrientation("1 1 N 1"));
		assertThrows(FileException.class, () -> InputFileParserHelper.parseMowerOrientation("1 1 L"));

		assertEquals(InputFileParserHelper.parseMowerOrientation("1 2 S"), Orientation.SOUTH);
	}

	@Test
	void parseMowerMovement() {
		assertThrows(FileException.class, () -> InputFileParserHelper.parseMowerMovement("1"));
		assertThrows(FileException.class, () -> InputFileParserHelper.parseMowerMovement("L R"));
		assertThrows(FileException.class, () -> InputFileParserHelper.parseMowerMovement("L, R"));
		assertThrows(FileException.class, () -> InputFileParserHelper.parseMowerMovement("L,R"));
		assertThrows(FileException.class, () -> InputFileParserHelper.parseMowerMovement("LRFRLN"));

		assertEquals(InputFileParserHelper.parseMowerMovement(null), new LinkedList<>());
		assertEquals(InputFileParserHelper.parseMowerMovement(""), new LinkedList<>());
		assertEquals(InputFileParserHelper.parseMowerMovement("FRFFLL"), new LinkedList<>(Arrays.asList(
				Movement.MOVE_FORWARD,
				Movement.TURN_RIGHT,
				Movement.MOVE_FORWARD,
				Movement.MOVE_FORWARD,
				Movement.TURN_LEFT,
				Movement.TURN_LEFT)));
	}
}