package com.louie.automow.io.output;

import com.louie.automow.io.FileException;
import com.louie.automow.lawn.Lawn;
import com.louie.automow.lawn.SquareLawn;
import com.louie.automow.movable.Movable;
import com.louie.automow.movable.Mower;
import com.louie.automow.movable.Orientation;
import com.louie.automow.position.Coordinate;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.LinkedList;
import java.util.List;

import static com.louie.automow.fixtures.TestFixtureHelpers.ACTUAL_OUTPUT_FIXTURES_DIR;
import static com.louie.automow.fixtures.TestFixtureHelpers.EXPECTED_OUTPUT_FIXTURES_DIR;
import static com.louie.automow.fixtures.TestFixtureHelpers.assertFilesMatch;
import static org.junit.jupiter.api.Assertions.assertThrows;

class OutputToFileSpec {
	private static final List<Movable> movables;

	static {
		movables = new LinkedList<>();
		Lawn lawn = new SquareLawn(new Coordinate(0, 0), new Coordinate(5, 5));
		movables.add(new Mower(lawn, new Coordinate(1, 3), Orientation.NORTH));
		movables.add(new Mower(lawn, new Coordinate(5, 1), Orientation.EAST));
	}

	@Test
	void outputToNonexistentDir() {
		Output<File> outputFileWriter = new OutputToFile();
		assertThrows(FileException.class, () -> outputFileWriter.output(new File(EXPECTED_OUTPUT_FIXTURES_DIR + "bad_dir/output"), new LinkedList<>()));
	}

	@Test
	void outputToNonexistentFile() throws IOException {
		final String nonexistentFile = ACTUAL_OUTPUT_FIXTURES_DIR + "nonexistent";
		File file  = new File(nonexistentFile);
		Files.deleteIfExists(file.toPath());
		Output<File> outputFileWriter = new OutputToFile();
		outputFileWriter.output(file, movables);

		assertFilesMatch(ACTUAL_OUTPUT_FIXTURES_DIR + "nonexistent",EXPECTED_OUTPUT_FIXTURES_DIR + "nonexistent");
	}

	@Test
	void outputToExistingFile() throws IOException {
		final String existingFile = ACTUAL_OUTPUT_FIXTURES_DIR + "existing";
		File file  = new File(existingFile);
		Output<File> outputFileWriter = new OutputToFile();
		outputFileWriter.output(file, movables);

		assertFilesMatch(ACTUAL_OUTPUT_FIXTURES_DIR + "existing",EXPECTED_OUTPUT_FIXTURES_DIR + "existing");
	}
}