package com.louie.automow.fixtures;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestFixtureHelpers {
	private static final String FIXTURES_DIR
	= System.getProperty("user.dir") + "/test/com/louie/automow/fixtures/";

	public static final String INPUT_FIXTURES_DIR = FIXTURES_DIR + "input/";
	public static final String EXPECTED_OUTPUT_FIXTURES_DIR = FIXTURES_DIR + "output/expected/";
	public static final String ACTUAL_OUTPUT_FIXTURES_DIR = FIXTURES_DIR + "output/actual/";

	public static void assertFilesMatch(String actual, String expected) throws IOException {
		try(BufferedReader brActual = new BufferedReader(new FileReader(new File(actual)))) {
			try(BufferedReader brExpected = new BufferedReader(new FileReader(new File(expected)))) {
				String actualLine = brActual.readLine();
				String expectedLine = brExpected.readLine();
				while(actualLine != null && expectedLine != null) {
					assertEquals(actualLine, expectedLine);
					actualLine = brActual.readLine();
					expectedLine = brExpected.readLine();
				}
			}
		}
	}
}