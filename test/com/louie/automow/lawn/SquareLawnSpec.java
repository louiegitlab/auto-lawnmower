package com.louie.automow.lawn;

import com.louie.automow.position.Coordinate;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class SquareLawnSpec {
	@Test
	void ThrowsLawnExceptionWhenInputsAreBad() {
		LawnException e = assertThrows(LawnException.class, () -> new SquareLawn(new Coordinate(0, 0), new Coordinate(2, -1)));
		assertEquals(e.getMessage(), "coordinate (2, -1) is not above and to the right of (0, 0)");

		e = assertThrows(LawnException.class, () -> new SquareLawn(new Coordinate(0, 0), new Coordinate(-2, 1)));
		assertEquals(e.getMessage(), "coordinate (-2, 1) is not above and to the right of (0, 0)");
	}

	@Test
	void isInsideBoundariesReturnsFalse() {
		Lawn lawn = new SquareLawn(new Coordinate(0, 0), new Coordinate(10, 10));
		assertFalse(lawn.isInsideBoundaries(new Coordinate(5, 11)));

		lawn = new SquareLawn(new Coordinate(-10, -10), new Coordinate(10, 10));
		assertFalse(lawn.isInsideBoundaries(new Coordinate(-5, 11)));
	}

	@Test
	void isInsideBoundariesReturnsTrue() {
		Lawn lawn = new SquareLawn(new Coordinate(0, 0), new Coordinate(0, 0));
		assertTrue(lawn.isInsideBoundaries(new Coordinate(0, 0)));

		lawn = new SquareLawn(new Coordinate(0, 0), new Coordinate(10, 10));
		assertTrue(lawn.isInsideBoundaries(new Coordinate(5, 9)));

		lawn = new SquareLawn(new Coordinate(-10, -10), new Coordinate(-1, -1));
		assertTrue(lawn.isInsideBoundaries(new Coordinate(-2, -4)));
	}
}