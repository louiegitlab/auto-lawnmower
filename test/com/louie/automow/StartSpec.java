package com.louie.automow;


import org.junit.jupiter.api.Test;

import java.io.IOException;

import static com.louie.automow.fixtures.TestFixtureHelpers.ACTUAL_OUTPUT_FIXTURES_DIR;
import static com.louie.automow.fixtures.TestFixtureHelpers.EXPECTED_OUTPUT_FIXTURES_DIR;
import static com.louie.automow.fixtures.TestFixtureHelpers.INPUT_FIXTURES_DIR;
import static com.louie.automow.fixtures.TestFixtureHelpers.assertFilesMatch;

class StartSpec {
	@Test
	void onlyLawn() throws IOException {
		String[] args = {INPUT_FIXTURES_DIR + "only_lawn", ACTUAL_OUTPUT_FIXTURES_DIR + "only_lawn"};
		Start.start(args);
		assertFilesMatch(
				ACTUAL_OUTPUT_FIXTURES_DIR + "only_lawn",
				EXPECTED_OUTPUT_FIXTURES_DIR + "only_lawn");
	}

	@Test
	void oneMower() throws IOException {
		String[] args = {INPUT_FIXTURES_DIR + "one_mower", ACTUAL_OUTPUT_FIXTURES_DIR + "one_mower"};
		Start.start(args);
		assertFilesMatch(
				ACTUAL_OUTPUT_FIXTURES_DIR + "one_mower",
				EXPECTED_OUTPUT_FIXTURES_DIR + "one_mower");
	}

	@Test
	void incompleteSecondMower() throws IOException {
		String[] args = {INPUT_FIXTURES_DIR + "incomplete_second_mower", ACTUAL_OUTPUT_FIXTURES_DIR + "incomplete_second_mower"};
		Start.start(args);
		assertFilesMatch(
				ACTUAL_OUTPUT_FIXTURES_DIR + "incomplete_second_mower",
				EXPECTED_OUTPUT_FIXTURES_DIR + "incomplete_second_mower");
	}

	@Test
	void twoMowers() throws IOException {
		String[] args = {INPUT_FIXTURES_DIR + "two_mowers", ACTUAL_OUTPUT_FIXTURES_DIR + "two_mowers"};
		Start.start(args);
		assertFilesMatch(
				ACTUAL_OUTPUT_FIXTURES_DIR + "two_mowers",
				EXPECTED_OUTPUT_FIXTURES_DIR + "two_mowers");
	}
}