**README**

*Running Prerequisites*

Before running/testing you'll need to do a few things:
1. Add JUnit 5.3 to your classpath. You should see it in 'External Libraries'.
2. Set Project SDK to 12 (Java version "12.0.1")
3. Set Project Language Level to "12 (Preview) Switch expressions"