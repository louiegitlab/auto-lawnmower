package com.louie.automow;

import com.louie.automow.io.input.InputDTO;
import com.louie.automow.lawn.Lawn;
import com.louie.automow.lawn.SquareLawn;
import com.louie.automow.movable.Movable;
import com.louie.automow.movable.Control;
import com.louie.automow.movable.Mower;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

final class AutoMow {
	static List<Movable> autoMow(InputDTO instructions) {
		Lawn lawn = new SquareLawn(instructions.bottomLeftOfLawn, instructions.topRightOfLawn);

		List<Control> remoteControls = instructions.mowerInstructionDTOs.stream()
			.map(mowerDTO -> {
				Mower mower = new Mower(lawn, mowerDTO.startingLocation, mowerDTO.orientation);
				return new Control(mower, mowerDTO.movement);
			})
			.collect(Collectors.toList());

		List<Movable> mowersInFinalPositions = new LinkedList<>();
		for(Control rc : remoteControls) {
			mowersInFinalPositions.add(rc.move());
		}
		return mowersInFinalPositions;
	}
}