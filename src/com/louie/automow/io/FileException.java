package com.louie.automow.io;

public class FileException extends RuntimeException {
	public FileException(String errorMessage, Throwable err) {
		super(errorMessage, err);
	}

	public FileException(String errorMessage) {
		super(errorMessage);
	}
}