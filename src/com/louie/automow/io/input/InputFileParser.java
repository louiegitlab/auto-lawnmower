package com.louie.automow.io.input;

import com.louie.automow.io.FileException;
import com.louie.automow.movable.Movement;
import com.louie.automow.movable.Orientation;
import com.louie.automow.position.Coordinate;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Queue;

public class InputFileParser implements InputParser<File> {
	public InputDTO parse(File file) {
		try(BufferedReader br = new BufferedReader(new FileReader(file))) {
			InputDTO.Builder builder = new InputDTO.Builder();
			builder = parseLawnCoordinates(builder, br.readLine());

			String mowerPositionLine = br.readLine();
			while (mowerPositionLine != null) {
				String mowerMovementLine = br.readLine();
				builder = parseMowerInstructions(builder, mowerPositionLine, mowerMovementLine);
				mowerPositionLine = br.readLine();
			}
			return builder.build();
		} catch(IOException e) {
			throw new FileException(String.format("Error while parsing file %s", file), e);
		}
	}

	private static InputDTO.Builder parseLawnCoordinates(InputDTO.Builder builder, String line) {
		return builder.addLawn(InputFileParserHelper.parseLawnCoordinates(line));
	}

	private static InputDTO.Builder parseMowerInstructions(InputDTO.Builder builder, String mowerPositionLine, String mowerMovementLine) {
		Coordinate mowerCoordinate = InputFileParserHelper.parseMowerCoordinates(mowerPositionLine);
		Orientation mowerOrientation = InputFileParserHelper.parseMowerOrientation(mowerPositionLine);
		Queue<Movement> mowerMovement = InputFileParserHelper.parseMowerMovement(mowerMovementLine);
		return builder.addMowerInstructions(mowerCoordinate, mowerOrientation, mowerMovement);
	}
}