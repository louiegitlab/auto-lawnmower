package com.louie.automow.io.input;

import com.louie.automow.movable.Movement;
import com.louie.automow.movable.Orientation;
import com.louie.automow.position.Coordinate;

import java.util.Queue;

public class MowerInputDTO {
	public final Coordinate startingLocation;
	public final Orientation orientation;
	public final Queue<Movement> movement;

	MowerInputDTO(final Coordinate startingLocation, final Orientation orientation, final Queue<Movement> movement) {
		this.startingLocation = startingLocation;
		this.orientation = orientation;
		this.movement = movement;
	}
}