package com.louie.automow.io.input;

import com.louie.automow.io.FileException;
import com.louie.automow.io.constants.FileConstants;
import com.louie.automow.movable.Movement;
import com.louie.automow.movable.Orientation;
import com.louie.automow.position.Coordinate;

import java.util.LinkedList;
import java.util.Queue;

import static com.louie.automow.movable.Movement.*;
import static com.louie.automow.movable.Orientation.*;

class InputFileParserHelper {
	static Coordinate parseLawnCoordinates(String line) {
		line = replaceNull(line);
		String[] coordinatesArray = (line + "").trim().split("\\s+");
		if(coordinatesArray.length != 2) {
			throw new FileException(String.format("Line '%s' invalid. Expected input of the format: 5 5", line));
		}
		try {
			return new Coordinate(Integer.parseInt(coordinatesArray[0]), Integer.parseInt(coordinatesArray[1]));
		} catch(NumberFormatException e) {
			throw new FileException(String.format("Line '%s' invalid. Expected input of the format: 5 5", line), e);
		}
	}

	static Coordinate parseMowerCoordinates(String line) {
		line = replaceNull(line);
		String[] coordinatesArray = parseMowerCoordinatesAndOrientation(line);
		try {
			return new Coordinate(Integer.parseInt(coordinatesArray[0]), Integer.parseInt(coordinatesArray[1]));
		} catch(NumberFormatException e) {
			throw new FileException(String.format("Line '%s' invalid. Expected input of the format: 1 2 N", line), e);
		}
	}

	static Orientation parseMowerOrientation(String line) {
		line = replaceNull(line);
		String[] orientationArray = parseMowerCoordinatesAndOrientation(line);
		return stringToOrientation(orientationArray[2]);
	}

	static Queue<Movement> parseMowerMovement(String line) {
		line = replaceNull(line);
		String[] coordinatesArray = line.trim().split("\\s+");
		if(coordinatesArray.length != 1) {
			throw new FileException(String.format("Line '%s' invalid. Expected input of the format: LFLFRFLFF", line));
		}
		return stringToMovementQueue(coordinatesArray[0]);
	}

	private static String[] parseMowerCoordinatesAndOrientation(String line) {
		String[] coordinatesAndOrientationArray = line.trim().split("\\s+");
		if(coordinatesAndOrientationArray.length != 3) {
			throw new FileException(String.format("Line '%s' invalid. Expected input of the format: 1 2 N", line));
		}
		return coordinatesAndOrientationArray;
	}

	private static Orientation stringToOrientation(String orientation) {
		switch(orientation) {
			case FileConstants.NORTH -> { return NORTH; }
			case FileConstants.EAST -> { return EAST; }
			case FileConstants.SOUTH -> { return SOUTH; }
			case FileConstants.WEST -> { return WEST; }
			default -> throw new FileException(String.format(
					"Invalid orientation. Orientation must be either %s, %s, %s, or %s",
					FileConstants.NORTH,
					FileConstants.EAST,
					FileConstants.SOUTH,
					FileConstants.WEST));
		}
	}

	private static Queue<Movement> stringToMovementQueue(String movement) {
		Queue<Movement> movementQueue = new LinkedList<>();
		for(char c : movement.toCharArray()) {
			movementQueue.add(charToMovement(c));
		}
		return movementQueue;
	}

	private static Movement charToMovement(final char input) {
		switch(String.valueOf(input)) {
			case FileConstants.FORWARD -> { return MOVE_FORWARD; }
			case FileConstants.LEFT -> { return TURN_LEFT; }
			case FileConstants.RIGHT -> { return TURN_RIGHT; }
			default -> throw new FileException(String.format(
					"Invalid movement. Movement must be either %s, %s, or %s",
					FileConstants.FORWARD,
					FileConstants.LEFT,
					FileConstants.RIGHT));
		}
	}

	private static String replaceNull(String input) {
		return input == null ? "" : input;
	}
}