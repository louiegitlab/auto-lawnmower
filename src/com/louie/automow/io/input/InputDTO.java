package com.louie.automow.io.input;

import com.louie.automow.movable.Movement;
import com.louie.automow.movable.Orientation;
import com.louie.automow.position.Coordinate;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

public class InputDTO {
	public final Coordinate bottomLeftOfLawn;
	public final Coordinate topRightOfLawn;
	public final List<MowerInputDTO> mowerInstructionDTOs;

	private InputDTO(Coordinate bottomLeftOfLawn, Coordinate topRightOfLawn, List<MowerInputDTO> mowerInstructionDTOs) {
		this.bottomLeftOfLawn = bottomLeftOfLawn;
		this.topRightOfLawn = topRightOfLawn;
		this.mowerInstructionDTOs = mowerInstructionDTOs;
	}

	public static class Builder {
		private Coordinate bottomLeftOfLawn;
		private Coordinate topRightOfLawn;
		private List<MowerInputDTO> mowerInstructionDTOs;

		public Builder() {
			this.mowerInstructionDTOs = new ArrayList<>();
		}

		public Builder addLawn(Coordinate topRight) {
			this.bottomLeftOfLawn = new Coordinate(0, 0);
			this.topRightOfLawn = topRight;
			return this;
		}

		public Builder addMowerInstructions(Coordinate startingLocation, Orientation orientation, Queue<Movement> movement) {
			this.mowerInstructionDTOs.add(new MowerInputDTO(startingLocation, orientation, movement));
			return this;
		}

		public InputDTO build() {
			return new InputDTO(
					this.bottomLeftOfLawn,
					this.topRightOfLawn,
					this.mowerInstructionDTOs
			);
		}
	}
}