package com.louie.automow.io.input;

public interface InputParser<T> {
	InputDTO parse(T input);
}
