package com.louie.automow.io.output;

import com.louie.automow.io.FileException;
import com.louie.automow.io.constants.FileConstants;
import com.louie.automow.movable.Movable;
import com.louie.automow.movable.Orientation;
import com.louie.automow.position.Coordinate;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class OutputToFile implements Output<File> {
	public void output(File file, List<Movable> movables) {
		try(FileWriter fw = new FileWriter(file)) {
			for(Movable movable : movables) {
				fw.write(String.format(
						"%s %s%n",
						coordinateToString(movable.getCoordinate()), orientationToString(movable.getOrientation())));
			}
		} catch(IOException e) {
			throw new FileException(String.format("Error while writing file %s", file), e);
		}
	}

	private static String orientationToString(Orientation orientation) {
		return switch(orientation) {
			case NORTH -> FileConstants.NORTH;
			case EAST -> FileConstants.EAST;
			case SOUTH -> FileConstants.SOUTH;
			case WEST -> FileConstants.WEST;
		};
	}

	private static String coordinateToString(Coordinate coordinate) {
		return String.format("%s %s", coordinate.x, coordinate.y);
	}
}