package com.louie.automow.io.output;

import com.louie.automow.movable.Movable;

import java.io.IOException;
import java.util.List;

public interface Output<T> {
	void output(T output, List<Movable> movables) throws IOException;
}
