package com.louie.automow;

import com.louie.automow.io.input.InputDTO;
import com.louie.automow.io.input.InputFileParser;
import com.louie.automow.io.output.OutputToFile;
import com.louie.automow.movable.Movable;

import java.io.File;
import java.util.List;

class Start {
	static void start(String[] args) {
		if(args.length != 2) throw new IllegalArgumentException("Expecting 2 file paths as arguments");

		File inputFile = new File(args[0]);
		File outputFile = new File(args[1]);

		InputDTO input = new InputFileParser().parse(inputFile);
		List<Movable> movables = AutoMow.autoMow(input);
		new OutputToFile().output(outputFile, movables);
	}
}