package com.louie.automow.lawn;

public class LawnException extends RuntimeException {
	public LawnException(String errorMessage, Throwable err) {
		super(errorMessage, err);
	}

	public LawnException(String errorMessage) {
		super(errorMessage);
	}
}