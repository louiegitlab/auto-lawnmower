package com.louie.automow.lawn;

import com.louie.automow.position.Coordinate;

public final class SquareLawn implements Lawn {
	private final Coordinate bottomLeft;
	private final Coordinate topRight;

	public SquareLawn(final Coordinate bottomLeft, final Coordinate topRight) {
		if(bottomLeft.x > topRight.x || bottomLeft.y > topRight.y) {
			throw new LawnException(String.format("coordinate %s is not above and to the right of %s", topRight, bottomLeft));
		}
		this.bottomLeft = bottomLeft;
		this.topRight = topRight;
	}

	public boolean isInsideBoundaries(final Coordinate coordinate) {
		return coordinate.x >= this.bottomLeft.x && coordinate.x <= this.topRight.x
			&& coordinate.y >= this.bottomLeft.y && coordinate.y <= this.topRight.y;
	}
}