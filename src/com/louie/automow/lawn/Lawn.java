package com.louie.automow.lawn;

import com.louie.automow.position.Coordinate;

public interface Lawn {
	boolean isInsideBoundaries(final Coordinate position);
}
