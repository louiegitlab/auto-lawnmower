package com.louie.automow.position;

import java.text.MessageFormat;
import java.util.Objects;

public final class Coordinate {
	public final int x;
	public final int y;

	public Coordinate(final int x, final int y) {
		this.x = x;
		this.y = y;
	}

	@Override
	public String toString() {
		return MessageFormat.format("({0}, {1})", this.x, this.y);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Coordinate that = (Coordinate) o;
		return x == that.x && y == that.y;
	}

	@Override
	public int hashCode() {
		return Objects.hash(x, y);
	}
}