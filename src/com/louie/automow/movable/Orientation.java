package com.louie.automow.movable;

public enum Orientation {
	NORTH(0),
	EAST(1),
	SOUTH(2),
	WEST(3);

	private final int direction;
	private static final Orientation[] orientationArray = new Orientation[] { NORTH, EAST, SOUTH, WEST };

	Orientation(final int direction) {
		this.direction = direction;
	}

	public static Orientation right90Degrees(final Orientation currentOrientation) {
		final int newOrientation = (currentOrientation.direction + 1) % 4;
		return orientationArray[newOrientation];
	}

	public static Orientation left90Degrees(final Orientation currentOrientation) {
		final int newOrientation = (4 + currentOrientation.direction - 1) % 4;
		return orientationArray[newOrientation];
	}
}