package com.louie.automow.movable;

import com.louie.automow.lawn.Lawn;
import com.louie.automow.position.Coordinate;

public final class Mower implements Movable {
	private Coordinate coordinate;
	private Orientation orientation;
	private Lawn lawn;

	public Mower(Lawn lawn, Coordinate coordinate, Orientation orientation) {
		if(!lawn.isInsideBoundaries(coordinate)) {
			throw new MovableException(String.format("Attempt to place mower %s outside of lawn", coordinate));
		}
		this.lawn = lawn;
		this.coordinate = coordinate;
		this.orientation = orientation;
	}

	@Override
	public Mower turnLeft() {
		this.orientation = Orientation.left90Degrees(orientation);
		return this;
	}

	@Override
	public Mower turnRight() {
		this.orientation = Orientation.right90Degrees(orientation);
		return this;
	}

	@Override
	public Mower moveForward() {
		Coordinate coordinate = this.coordinateInFront();
		if(lawn.isInsideBoundaries(coordinate)) {
			this.coordinate = coordinate;
		}
		return this;
	}

	public Coordinate getCoordinate() { return coordinate; }

	public Orientation getOrientation() { return orientation; }

	private Coordinate coordinateInFront() {
		return switch (this.orientation) {
			case NORTH -> new Coordinate(coordinate.x, coordinate.y + 1);
			case EAST -> new Coordinate(coordinate.x + 1, coordinate.y);
			case SOUTH -> new Coordinate(coordinate.x, coordinate.y - 1);
			case WEST -> new Coordinate(coordinate.x - 1, coordinate.y);
		};
	}
}