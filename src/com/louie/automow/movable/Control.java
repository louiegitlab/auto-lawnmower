package com.louie.automow.movable;

import java.util.LinkedList;
import java.util.Queue;

public class Control {
	private Queue<Movement> movements;
	private final Movable movable;

	public Control(final Movable movable, Queue<Movement> movements) {
		this.movable = movable;
		this.movements = movements;
	}

	public Control(final Movable movable) {
		this(movable, new LinkedList<>());
	}

	public void queueMovement(Movement movement) {
		this.movements.add(movement);
	}

	public void queueMovement(Queue<Movement> movements) {
		this.movements.addAll(movements);
	}

	public Movable move() {
		while(!movements.isEmpty()) {
			switch(movements.remove()) {
				case MOVE_FORWARD -> this.movable.moveForward();
				case TURN_LEFT -> this.movable.turnLeft();
				case TURN_RIGHT -> this.movable.turnRight();
			}
		}
		return this.movable;
	}

	public Queue<Movement> getMovements() {
		return movements;
	}

	public Movable getMovable() {
		return movable;
	}
}