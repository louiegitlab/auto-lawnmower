package com.louie.automow.movable;

public class MovableException extends RuntimeException {
	public MovableException(String errorMessage, Throwable err) {
		super(errorMessage, err);
	}

	public MovableException(String errorMessage) {
		super(errorMessage);
	}
}