package com.louie.automow.movable;

public enum Movement {
	MOVE_FORWARD,
	TURN_LEFT,
	TURN_RIGHT
}