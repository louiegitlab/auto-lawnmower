package com.louie.automow.movable;

import com.louie.automow.position.Coordinate;

public interface Movable {
	Movable moveForward();
	Movable turnLeft();
	Movable turnRight();
	Orientation getOrientation();
	Coordinate getCoordinate();
}
